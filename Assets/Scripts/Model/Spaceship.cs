﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship {

    public EnumerationHelper.SpaceshipName name;
    public int numberOfShields;
    public static EnumerationHelper.SpaceshipName[] spaceshipArray = new EnumerationHelper.SpaceshipName[3]{
        EnumerationHelper.SpaceshipName.Skylark,
        EnumerationHelper.SpaceshipName.Nsonar,
        EnumerationHelper.SpaceshipName.Moontaker };

    public Spaceship(EnumerationHelper.SpaceshipName newName) {
        numberOfShields = 5;
        name = newName;
    }

    public void UseShield(){
        if (WaveManager.waveManager.currentWaveNumber > -1){
            numberOfShields--;
        }
    }

    public void AddShield(){
        numberOfShields++;
    }
}
