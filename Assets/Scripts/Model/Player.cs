﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player{

    public static Player player = new Player();
    public Spaceship currentSpaceship;
    public int topScore;
    public bool hasNewTopScore;
    int _currentScore;
    public int currentScore {
        get {
            return _currentScore;
        }
        set{
            _currentScore = value;
            CheckScoreThreshold();
        }
    }
    public List<EnumerationHelper.SectionPlace> inputMovementsList { get; set; }
    public bool isSpaceshipAlive;
    public int Score;
    readonly int DEFAULT_SCORE_THRESHOLD = 100;
    public int currentScoreTreshold;

    public Player() {
        currentScoreTreshold = DEFAULT_SCORE_THRESHOLD;
        currentScore = topScore = 0;
        currentSpaceship = new Spaceship(EnumerationHelper.SpaceshipName.Nsonar);
        isSpaceshipAlive = true;
        hasNewTopScore = false;
    }
    
    public void setDefaultSpaceship(){
        hasNewTopScore = false;
        currentScore = 0;
        currentSpaceship.name = EnumerationHelper.SpaceshipName.Nsonar;
    }

    public void ChangeSpaceship(int direction) { 
        for (int i=0 ; i<Spaceship.spaceshipArray.Length ; i++){
            if (currentSpaceship.name == Spaceship.spaceshipArray[i]){
                if (i+direction < 0){
                    currentSpaceship = new Spaceship(Spaceship.spaceshipArray[Spaceship.spaceshipArray.Length - 1]);
                    break;
                } else if (i+direction == Spaceship.spaceshipArray.Length){
                    currentSpaceship = new Spaceship(Spaceship.spaceshipArray[0]);
                    break;
                } else {
                    currentSpaceship = new Spaceship(Spaceship.spaceshipArray[i + direction]);
                    break;
                }
            }
        }
	}

    public void AddPointsToScore(int pointsToAdd) { 
        currentScore += pointsToAdd;
        if (currentScore > topScore) {
            topScore = currentScore;
            hasNewTopScore = true;
        }
    }
    
    public void CheckScoreThreshold(){
        if (currentScore >= currentScoreTreshold){
            currentSpaceship.AddShield();
            currentScoreTreshold += DEFAULT_SCORE_THRESHOLD;
        }
    }
}
