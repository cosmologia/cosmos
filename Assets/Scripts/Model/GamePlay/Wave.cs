﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave {

    public Section[] sectionArray;

    public Wave(int numberOfSections) {
        sectionArray = new Section[numberOfSections];
    }

    public void FillSections(int maxRewards) {
        int currentRewards = 0;
        DefineSolutionPath();
        for (int i = 0; i < sectionArray.Length; i++) {
            sectionArray[i].FillObstacles();
            if (currentRewards < maxRewards){
                currentRewards += sectionArray[i].FillRestOfPlaces(maxRewards - currentRewards);
            }
        }
	}

    void DefineSolutionPath() {
        sectionArray[0] = new Section();
        for (int i = 1; i < sectionArray.Length; i++) {
            sectionArray[i] = new Section(sectionArray[i-1]);
        }
    }

    public void ChangeAnticosmosSections(){
        for (int i=0 ; i<sectionArray.Length ; i++){
            if (sectionArray[i].isAnticosmosHorizontal){
                sectionArray[i].RotateHorizontally();
            } else if (sectionArray[i].isAnticosmosVertical){
                sectionArray[i].RotateVertically();
            }
        }
    }

	public bool[] GetPatternResolutionVerification() {
		bool[] patternResolutionVerification = new bool[sectionArray.Length];
        EnumerationHelper.SectionPlace lastPlayerMovement = EnumerationHelper.SectionPlace.Center;

        if (Player.player.inputMovementsList.Count < sectionArray.Length && Player.player.inputMovementsList.Count > 0){
            lastPlayerMovement = Player.player.inputMovementsList[Player.player.inputMovementsList.Count - 1];
        }

		for (int i=0 ; i<sectionArray.Length ; i++) {
            if (Player.player.inputMovementsList.Count <= i){
                patternResolutionVerification[i] = sectionArray[i].ElementInPlaceIsEnemy(lastPlayerMovement);
                Player.player.inputMovementsList.Add(lastPlayerMovement);
            } else {
                patternResolutionVerification[i] = sectionArray[i].ElementInPlaceIsEnemy(Player.player.inputMovementsList[i]);
            }
        }

		return patternResolutionVerification;
	}
}
