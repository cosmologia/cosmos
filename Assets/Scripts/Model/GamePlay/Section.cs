﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Section {
    
    readonly int minObstacles = 5;
    readonly int maxObstacles = 8;
    public readonly Vector2 solutionPos;
    public int pointValue = 0;
    public bool isAnticosmosHorizontal;
    public bool isAnticosmosVertical;

    public SectionElement[,] elementArray = new SectionElement[3,3];

    public static readonly EnumerationHelper.SectionPlace[,] placeArray = new EnumerationHelper.SectionPlace[3,3] { 
		{ EnumerationHelper.SectionPlace.TopLeft, EnumerationHelper.SectionPlace.Top, EnumerationHelper.SectionPlace.TopRight },
		{ EnumerationHelper.SectionPlace.CenterLeft, EnumerationHelper.SectionPlace.Center, EnumerationHelper.SectionPlace.CenterRight },
        { EnumerationHelper.SectionPlace.BottomLeft, EnumerationHelper.SectionPlace.Bottom, EnumerationHelper.SectionPlace.BottomRight }
    };

    public Section() {
        SetAnticosmos();
        solutionPos = new Vector2(Random.Range(0, elementArray.GetLength(0)), 
            Random.Range(0, elementArray.GetLength(1)));
     }

    public Section(Section sectionAhead) {
        SetAnticosmos();
        solutionPos = ChooseRandomSolutionPosition((int)sectionAhead.solutionPos.x, (int)sectionAhead.solutionPos.y);
    }
    
    void SetAnticosmos(){
        int randomTmp = Random.Range(0,6);
        if (randomTmp == 0){
            isAnticosmosHorizontal = true;
        } else if (randomTmp == 1){
            isAnticosmosVertical = true;
        }              
    }
    
    Vector2 ChooseRandomSolutionPosition(int x, int y) {
        List<Vector2> PossibleSolutionPositionList = GetPossibleSolutionPositions(x, y);
        return PossibleSolutionPositionList[Random.Range(0,PossibleSolutionPositionList.Count)];
    }

    List<Vector2> GetPossibleSolutionPositions(int x, int y) {
        List<Vector2> PossibleSolutionPositionList = new List<Vector2>();
        for (int i = (x - 1); i < (x + 1); i++) {
            if (i >= 0 && i < elementArray.GetLength(0)) {
                for (int j = (y - 1); j < (y + 1); j++) {
                    if (j >= 0 && j < elementArray.GetLength(0)) {
                        PossibleSolutionPositionList.Add(new Vector2(i, j));
                    }
                }
            }
        }
        return PossibleSolutionPositionList;
    }

    public void FillObstacles() {
        int currentNumberObstacles = 0;
        do {
            for (int i = 0; i < elementArray.GetLength(0); i++){
                for (int j = 0; j < elementArray.GetLength(1); j++){
                    if (elementArray[i, j] == null) { 
                        if (Random.Range(0, 2) == 0) {
                            if (i != solutionPos.x || j != solutionPos.y) { 
                                // If there are many obstacles, then call the method for choosing between them here
                                elementArray[i, j] = new Common();
                                pointValue += elementArray[i, j].pointValue;
                                currentNumberObstacles++;
                            }
                        }                        
                        if (currentNumberObstacles >= maxObstacles) { 
                            break;
                        } 
                    }
                }
            }
        } while (currentNumberObstacles < minObstacles);
    }

    public int FillRestOfPlaces(int maxReward) {
        int currentRewards = 0;
        for (int i = 0; i < elementArray.GetLength(0); i++) {
            for (int j = 0; j < elementArray.GetLength(1); j++) { 
	            if (elementArray[i, j] == null) {
                    if (currentRewards >= maxReward) {
                        return currentRewards;
                    } else if (Random.Range(0, 3) == 1) { 
                        elementArray[i, j] = new Reward();
                        pointValue += elementArray[i, j].pointValue;
                        currentRewards++;
                    }
	            }
			}
        }
        return currentRewards;
    }

    public bool ElementInPlaceIsEnemy(EnumerationHelper.SectionPlace place) { 
        for (int i = 0; i < elementArray.GetLength(0); i++) {
            for (int j = 0; j < elementArray.GetLength(1); j++) {
                if (placeArray[i, j] == place) {
                    if (elementArray[i,j] == null){
                        return false;
                    } else {
                        return elementArray[i, j].isEnemy;
                    }
                }
            }
        }
        return false;
    }
    
    public void RotateHorizontally(){
        SectionElement tempSectionElement;
        for (int i=0 ; i<elementArray.GetLength(0) ; i++){
            tempSectionElement = elementArray[i,0];
            elementArray[i,0] = elementArray[i,2];
            elementArray[i,2] = tempSectionElement;
        }
    }
    
    public void RotateVertically(){
        SectionElement tempSectionElement;
        for (int i=0 ; i<elementArray.GetLength(0) ; i++){
            tempSectionElement = elementArray[0,i];
            elementArray[0,i] = elementArray[2,i];
            elementArray[2,i] = tempSectionElement;
        }
    }      
}
