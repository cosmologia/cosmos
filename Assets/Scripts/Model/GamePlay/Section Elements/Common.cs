﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Common : SectionElement {

    public Common() {
        tag = EnumerationHelper.SectionElementTag.Common;
        pointValue = 1;
        isEnemy = true;
    }
}
