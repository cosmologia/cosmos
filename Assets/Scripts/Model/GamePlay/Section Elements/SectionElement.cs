﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SectionElement{

    public EnumerationHelper.SectionElementTag tag = EnumerationHelper.SectionElementTag.Empty;
    public int pointValue = 1;
    public bool isEnemy = false;
}
