﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward : SectionElement {

	public int multiplier;
	int maxProbabilityValue = 100;
	Vector2[] probabilityArray = new Vector2[3] {
		new Vector2 (0, 60),
		new Vector2 (60, 90),
		new Vector2 (90, 100)
	};

	public Reward(){
		multiplier = setMultiplier();
        pointValue = 1;
        pointValue = pointsGained ();
		tag = EnumerationHelper.SectionElementTag.Reward;
        isEnemy = false;
    }

	public int pointsGained(){
		pointValue *= setMultiplier();
		return pointValue;
	}

	public int setMultiplier(){
		int tempRandom = Random.Range (0,maxProbabilityValue);	

		for (int i = 0; i < probabilityArray.Length; i++) {
			if (tempRandom >= probabilityArray [i].x && tempRandom < probabilityArray [i].y) {
				return i + 1;
			}
		}
		return 0;
	}

}
