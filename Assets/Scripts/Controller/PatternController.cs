﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PatternController : MonoBehaviour {

    public static PatternController patternController;
    GameObject[,] formattedinputArray;
    public GameObject[] inputArray;
    public GameObject buttonsParent;
	int listMax;

    void Awake(){
        if (patternController == null) {
            patternController = this;
        } else {
            Destroy(gameObject);
        }
		ConvertTo2DArray ();
		AssignCustomListener();
        CancelInvoke("StopReceivingInputs");
    }
    
    public void ReceiveInputs(){
        buttonsParent.SetActive(true);
        if (WaveManager.waveManager.currentWaveNumber < 1){
            listMax = WaveManager.waveManager.currentWave.sectionArray.Length;
        } else {
            listMax = WaveManager.waveManager.currentSectionNumber;
        }
        Player.player.inputMovementsList = new List<EnumerationHelper.SectionPlace>();
		for (int i = 0; i < formattedinputArray.GetLength (0); i++) {
			for (int j = 0; j < formattedinputArray.GetLength (1); j++) {
				formattedinputArray [i, j].GetComponent<Button> ().interactable = true;
                ChangeInputVisibility(formattedinputArray [i, j], true);
			}
		}
        Invoke("StopReceivingInputs", WaveManager.waveManager.currentPatternTime);
    }
    
    public void StopReceivingInputs(){
        GameManager.gameManager.currentState.EndHandle();
        buttonsParent.SetActive(false);
    }
	
    void ConvertTo2DArray()  {
        formattedinputArray = new GameObject[3, 3];
        int tempIterator = 0;
        for (int i = 0; i < formattedinputArray.GetLength(0) ; i++){
            for (int j = 0; j < formattedinputArray.GetLength(1) ; j++) {
                formattedinputArray[i, j] = inputArray[tempIterator];
                tempIterator++;
            }
        }
    }

    void AssignCustomListener() {
        Button tempButton;
        for (int i = 0; i < formattedinputArray.GetLength(0) ; i++){
            for (int j = 0; j < formattedinputArray.GetLength(1) ; j++) {
                tempButton = formattedinputArray[i, j].GetComponent<Button>();
                EnumerationHelper.SectionPlace tempPlace = Section.placeArray[i, j]; 
                tempButton.onClick.AddListener(delegate { SaveInput(tempPlace); } );
            }
        }
    }

    void SaveInput(EnumerationHelper.SectionPlace place) {
		AudioView.audioView.PlaySFX (3);
		UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().MoveSectionArrow();
		Vector2 clickedBtn = new Vector2 ();
        Player.player.inputMovementsList.Add(place);
		for (int i = 0; i < Section.placeArray.GetLength (0); i++) {
			for (int j = 0; j < Section.placeArray.GetLength (1); j++) {
				formattedinputArray [i, j].GetComponent<Button> ().interactable = false;
                ChangeInputVisibility(formattedinputArray [i, j], false);
				if (Section.placeArray [i, j] == place) {
					clickedBtn = new Vector2 (i, j);
				}
			}
		}
		InitButton ((int)clickedBtn.x, (int)clickedBtn.y);
        if (Player.player.inputMovementsList.Count >= listMax) {
            GameManager.gameManager.currentState.EndHandle();
            CancelInvoke("StopReceivingInputs");
            buttonsParent.SetActive(false);
        }
    }

	void InitButton(int x, int y) {
		for (int i = (x - 1); i <= (x + 1); i++) {
			if (i >= 0 && i < formattedinputArray.GetLength(0)) {
		        for (int j = (y - 1); j <= (y + 1); j++) {
                    if (j >= 0 && j < formattedinputArray.GetLength(1)) {
                        formattedinputArray [i, j].GetComponent<Button> ().interactable = true;
                        ChangeInputVisibility(formattedinputArray [i, j], true);
                    }
	            }
            }
	    }
	}
    
    void ChangeInputVisibility(GameObject button, bool showing){
        RectTransform buttonRect = button.GetComponent<RectTransform>();
        Vector3 startScale = (showing)? Vector3.zero : Vector3.one;
        Vector3 targetScale = (showing)? Vector3.one : Vector3.zero;
        buttonRect.localScale = startScale;
        buttonRect.DOScale(targetScale, .4f).SetEase(Ease.OutExpo);
        
        Image buttonImage = button.GetComponent<Image>();
        float startAlpha = (showing)? 0f : 1f;
        float targetAlpha = (showing)? 1f : 0f;
        buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, startAlpha);
        buttonImage.DOFade(targetAlpha, .4f).SetEase(Ease.OutExpo);
    }
}
