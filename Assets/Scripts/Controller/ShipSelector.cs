﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShipSelector : MonoBehaviour {

	const int angleVariation = 120;
	const int offset = 15;

	public float speed;
	public ShipRotation shipRot;
	public Image inputBlockerOverlayImage;

	Quaternion startRot;
	Quaternion endRot;
	Vector3 initTouchPos;
	Vector3 vectorDifference;
	Vector3 endTouch;

	public float[] pivotRotations;
	int currentRotationIndex;


	// Use this for initialization
	void Start () {
		Player.player.setDefaultSpaceship ();
	}

	// Update is called once per frame
	void Update () {

		if (Input.touchCount <= 0)
			return;
		if (Input.GetTouch (0).phase == TouchPhase.Began) {
			initTouchPos = Input.GetTouch (0).position;
		}

		if (Input.GetTouch (0).phase == TouchPhase.Ended) {
			endTouch = Input.GetTouch (0).position;
			SetDirection (initTouchPos, endTouch);
		}
	}

	void SetDirection (Vector3 firstTouch, Vector3 endTouch) {
		if (endTouch.x > firstTouch.x + offset) {
			Rotate (1);
		} else if (endTouch.x < firstTouch.x - offset) {
			Rotate (-1);
		}
	}

	public void Rotate (int direction) { // 1 = right, -1 = left
		inputBlockerOverlayImage.raycastTarget = true;
		AudioView.audioView.PlaySFX (1);
		startRot = transform.rotation;

		currentRotationIndex += direction;
		if (currentRotationIndex > pivotRotations.Length - 1)
			currentRotationIndex = 0;
		else if (currentRotationIndex < 0)
			currentRotationIndex = pivotRotations.Length - 1;

		Vector3 target = new Vector3 (startRot.eulerAngles.x, pivotRotations[currentRotationIndex], startRot.eulerAngles.z);
		Player.player.ChangeSpaceship (direction * -1);
		shipRot.Turn (Player.player.currentSpaceship.name.ToString (), target);
	}

	public void PressStart () {
		inputBlockerOverlayImage.raycastTarget = true;
		shipRot.StartGame ();
	}
}