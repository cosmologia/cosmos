﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public IGameManagerState currentState { get; set; }
    bool _canHandleState;

    public static bool firstTimePlaying=true;

    public bool canHandleState {
        get { return _canHandleState; }
        set{
            _canHandleState = value;
            if (_canHandleState == true) {
                currentState.Handle();
            }
        }
    }
    public bool isBeginningNewWave { get; set; }

    void ChangeState(IGameManagerState newState){
        currentState = newState;
    }
	
    void Awake(){
        if (gameManager == null) {
            gameManager = this;
        } else {
            Destroy(gameObject);
        }

    }
	
    void Start () { 
        StartGame();
    }

    public void StartGame(){
        isBeginningNewWave = true;
        WaveManager.waveManager.SetDefaultValues();
        currentState = new FreeTravelState();
        canHandleState = true;
    }
}
