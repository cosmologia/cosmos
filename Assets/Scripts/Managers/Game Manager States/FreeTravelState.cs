﻿using DG.Tweening;
using UnityEngine;

public class FreeTravelState : IGameManagerState {

    public void Handle () {
        GameManager.gameManager.canHandleState = false;
        if (GameManager.gameManager.isBeginningNewWave) {
            UIManager.uiManager.ppHandler.StopLensDistortionAnimation (.5f, Ease.OutExpo);
            if (WaveManager.waveManager.currentWaveNumber == 0)
                UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ShowSidePanels ();
            else {
                if (WaveManager.waveManager.currentWaveNumber > 0) {
                    if (WaveManager.waveManager.IncreaseRandomDifficulty ())
                        GamePlayViewManager.gamePlayViewManager.AnimateDifficultyText (WaveManager.waveManager.lastIncreasedDifficulty);
                } else {
                    GamePlayViewManager.gamePlayViewManager.AnimateDifficultyText (-1);
                }
            }
            GamePlayViewManager.gamePlayViewManager.DecreasePacing ();
        } else {
            GamePlayViewManager.gamePlayViewManager.IncreasePacing ();
        }
    }

    public void EndHandle () {
        if (GameManager.gameManager.isBeginningNewWave) {
            GameManager.gameManager.isBeginningNewWave = false;
            if (WaveManager.waveManager.currentWaveNumber < 0) {
                GameManager.gameManager.currentState = new TutorialState ();
            } else {
                WaveManager.waveManager.BuildWave ();
                GameManager.gameManager.currentState = new WaveVisualizationState ();
            }
        } else {
            GameManager.gameManager.currentState = new PatternResolutionState ();
        }
        GameManager.gameManager.canHandleState = true;
    }
}