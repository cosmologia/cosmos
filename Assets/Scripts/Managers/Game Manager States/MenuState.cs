﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuState : IGameManagerState {

	public void Handle(){
        GameManager.gameManager.canHandleState = false;
    }
    
    public void EndHandle(){
        GameManager.gameManager.currentState = new FreeTravelState();
        GameManager.gameManager.canHandleState = true;
    }
}
