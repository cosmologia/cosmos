﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternResolutionState : IGameManagerState {

    public void Handle () {
        GameManager.gameManager.canHandleState = false;
        GamePlayViewManager.gamePlayViewManager.StartWaveResolution ();
    }

    public void EndHandle () {
        if (Player.player.isSpaceshipAlive) {
            GameManager.gameManager.isBeginningNewWave = true;
            GameManager.gameManager.currentState = new FreeTravelState ();
            GameManager.gameManager.canHandleState = true;
        } else {
            UIManager.uiManager.HidePanels ();
            UIManager.uiManager.ScoreUI.GetComponent<ScoreUI> ().UpdateScore ();
            UIManager.uiManager.ChangeUIVisibility (UIManager.uiManager.ScoreUI, true);
        }
    }
}