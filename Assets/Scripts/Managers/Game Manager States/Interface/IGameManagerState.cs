﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameManagerState {

    void Handle ();

    void EndHandle ();
}