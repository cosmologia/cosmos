﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialState : IGameManagerState {

    public void Handle () {
        GameManager.gameManager.canHandleState = false;
        if (WaveManager.waveManager.currentWaveNumber == -3) {
            WaveManager.waveManager.SetDefaultValues ();
            WaveManager.waveManager.CreatePredefinedSections ();
        }

        switch (WaveManager.waveManager.currentWaveNumber) {
            case -3:
                WaveManager.waveManager.AssignSectionsToCurrentWave (
                    new Section[1] { WaveManager.waveManager.firstSection });
                break;
            case -2:
                WaveManager.waveManager.AssignSectionsToCurrentWave (
                    new Section[2] { WaveManager.waveManager.secondSection, WaveManager.waveManager.thirdSection });
                break;
            case -1:
                WaveManager.waveManager.AssignSectionsToCurrentWave (
                    new Section[2] { WaveManager.waveManager.fourthSection, WaveManager.waveManager.fifthSection });
                break;
        }
        WaveManager.waveManager.currentWaveNumber++;
        EndHandle ();
    }

    public void EndHandle () {
        GameManager.gameManager.currentState = new WaveVisualizationState ();
        GameManager.gameManager.canHandleState = true;
    }
}