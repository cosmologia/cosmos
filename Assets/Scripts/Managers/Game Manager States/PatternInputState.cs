﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PatternInputState : IGameManagerState {

    public void Handle () {
        GameManager.gameManager.canHandleState = false;
        PatternController.patternController.ReceiveInputs ();
        UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ShowRemainingTime (WaveManager.waveManager.currentPatternTime);
        UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ResetSectionArrow ();
    }

    public void EndHandle () {
        if (WaveManager.waveManager.currentWaveNumber < 0){
            UIManager.uiManager.ToggleBottomPanel(false);
        }
        UIManager.uiManager.ChangeUIVisibility (UIManager.uiManager.GameplayUI, false);
        GameManager.gameManager.currentState = new FreeTravelState ();
        GameManager.gameManager.canHandleState = true;
        UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ShowScoreOnBottomPanel (true);
        UIManager.uiManager.ToggleTopPanel (true);
        UIManager.uiManager.ppHandler.AnimateLensDistortion(70, 70, 1f, Ease.Linear, 99999f, 20f, Ease.Linear);
    }
}