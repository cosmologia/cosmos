﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class WaveVisualizationState : IGameManagerState {

    GameplayDataUI gameplayDataUI;

    public WaveVisualizationState () {
        UIManager.uiManager.ToggleTopPanel (false);
        gameplayDataUI = UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ();
    }

    public void Handle () {
        GameManager.gameManager.canHandleState = false;

        if (WaveManager.waveManager.currentWaveNumber <= 0)
            UIManager.uiManager.ToggleBottomPanel (true);

        UIManager.uiManager.ChangeUIVisibility (UIManager.uiManager.GameplayUI, true);
        gameplayDataUI.ShowScoreOnBottomPanel (false);
        gameplayDataUI.ShowNumberofSections (WaveManager.waveManager.currentWave.sectionArray.Length);
        GamePlayViewManager.gamePlayViewManager.StartWaveVisualization ();
    }

    public void EndHandle () {
        GameManager.gameManager.currentState = new PatternInputState ();
        GameManager.gameManager.canHandleState = true;
    }
}