﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayViewManager : MonoBehaviour {

    public static GamePlayViewManager gamePlayViewManager;

    public const float SECTION_SHOW_TIME_PERCENTAGE = .05f;
    public const float SECTION_HIDE_TIME_PERCENTAGE = .1f;
    public const float VISIBILITY_DURATION_TIME_PERCENTAGE = .85f;
    public const float PACING_CHANGE_TIME = 3.5f;

    public CameraMovement cameraMain;
    public SectionView section2D;
    public SectionView section3D;
    public SpaceshipView currentSpaceship;
    public StarsView starsView;
    public AnticosmosView anticosmos;
    public RectTransform difficultyTextRect;
    public Text difficultyText;

    void Awake () {
        if (gamePlayViewManager == null) {
            gamePlayViewManager = this;
        } else {
            Destroy (gameObject);
        }
    }

    #region WAVE RESOLUTION

    public void StartWaveResolution () {
        const float SECTION_RESOLUTION_VISIBILITY_TIME = 4f;
        StartCoroutine (ShowPatternResolutionWithinTime (WaveManager.waveManager.currentWave, SECTION_RESOLUTION_VISIBILITY_TIME));
    }

    IEnumerator ShowPatternResolutionWithinTime (Wave currentWave, float sectionVisibilityTime) {
        bool[] enemyInPositionArray = currentWave.GetPatternResolutionVerification ();
        for (int i = 0; i < currentWave.sectionArray.Length; i++) {
            if (currentWave.sectionArray[i] != null) {

                currentSpaceship.Move (Player.player.inputMovementsList[i]);
                section3D.ShowSectionElements (currentWave.sectionArray[i]);
                CheckAnticosmosActivation (currentWave.sectionArray, i);
                EvaluateCurrrentResolution (enemyInPositionArray[i]);
                cameraMain.SelectAnimationAccordingPosition (Player.player.inputMovementsList[i]);

                yield return new WaitWhile (() => SpaceshipView.hasCollided == false); // SPACESHIP HAS COLLIDED WITH SECTION3D COLLIDER

                SpaceshipView.hasCollided = false;
                section3D.HideSectionElements ();
                CheckAnticosmosDeactivation (currentWave.sectionArray, i);

                yield return new WaitWhile (() => ((SectionView3D) section3D).hiding3DSection); // SECTION HAS ENDED

                currentSpaceship.DeactivateShield ();
                if (Player.player.isSpaceshipAlive == false) {
                    i = enemyInPositionArray.Length;
                } else {
                    Player.player.AddPointsToScore (currentWave.sectionArray[i].pointValue);
                }
            }
        }
        cameraMain.ChangeToDefaultPosition ();
        currentSpaceship.Move (EnumerationHelper.SectionPlace.Center);
        GameManager.gameManager.currentState.EndHandle ();
    }

    void CheckAnticosmosActivation (Section[] sectionArray, int currentSectionPosition) {
        if (sectionArray[currentSectionPosition].isAnticosmosHorizontal || sectionArray[currentSectionPosition].isAnticosmosVertical) {
            if (currentSectionPosition > 0) {
                if (sectionArray[currentSectionPosition - 1].isAnticosmosHorizontal == false &&
                    sectionArray[currentSectionPosition - 1].isAnticosmosVertical == false) {
                    anticosmos.ChangeNegativeFilterGradually (true);
                }
            } else {
                anticosmos.ChangeNegativeFilterGradually (true);
            }
        }
    }

    void CheckAnticosmosDeactivation (Section[] sectionArray, int currentSectionPosition) {
        if (sectionArray[currentSectionPosition].isAnticosmosHorizontal || sectionArray[currentSectionPosition].isAnticosmosVertical) {
            if ((currentSectionPosition + 1 < sectionArray.Length &&
                    (sectionArray[currentSectionPosition + 1].isAnticosmosHorizontal == false && sectionArray[currentSectionPosition + 1].isAnticosmosVertical == false)) ||
                (currentSectionPosition == (sectionArray.Length - 1))) {
                anticosmos.ChangeNegativeFilterGradually (false);
            }
        }
    }

    void EvaluateCurrrentResolution (bool sectionResolution) {
        if (sectionResolution == true) {
            if (Player.player.currentSpaceship.numberOfShields > 0) {
                currentSpaceship.ActivateShield ();
            } else {
                Player.player.isSpaceshipAlive = false;
            }
        }
    }

    #endregion

    #region WAVE VISUALIZATION

    public void StartWaveVisualization () {
        StartCoroutine (ShowWaveSectionsWithinTime (WaveManager.waveManager.currentWave, WaveManager.waveManager.currentSectionVisibilityTime));
    }

    IEnumerator ShowWaveSectionsWithinTime (Wave currentWave, float sectionVisibilityTime) {
        currentWave.ChangeAnticosmosSections ();
        for (int i = 0; i < currentWave.sectionArray.Length; i++) {
            section2D.ShowSectionElements (currentWave.sectionArray[i]);
            UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ShowRemainingTime (WaveManager.waveManager.currentSectionVisibilityTime);
            UIManager.uiManager.GameplayDataUI.GetComponent<GameplayDataUI> ().ShowCurrentSection (i);
            //Debug.Log (sectionVisibilityTime * (SECTION_SHOW_TIME_PERCENTAGE + VISIBILITY_DURATION_TIME_PERCENTAGE));
            yield return new WaitForSeconds (sectionVisibilityTime * (SECTION_SHOW_TIME_PERCENTAGE + VISIBILITY_DURATION_TIME_PERCENTAGE));
            section2D.HideSectionElements ();
            yield return new WaitForSeconds (sectionVisibilityTime * SECTION_HIDE_TIME_PERCENTAGE);
        }
        currentWave.ChangeAnticosmosSections ();
        ((SectionView2D) section2D).SetDefaultValues ();
        GameManager.gameManager.currentState.EndHandle ();
    }

    #endregion

    #region PACING CONTROL METHODS

    public void DecreasePacing () {
        StartCoroutine (DecreasePacingWithinTime ());
    }

    IEnumerator DecreasePacingWithinTime () {
        yield return new WaitForSeconds (PACING_CHANGE_TIME * .3f);
        currentSpaceship.ChangeSpeed (EnumerationHelper.SpaceshipSpeedCommands.Deaccelerate);
        if (starsView.simulatingFast) {
            starsView.ChangeParticleSystemSpeed (false, PACING_CHANGE_TIME * .5f);
        }
        yield return new WaitForSeconds (PACING_CHANGE_TIME * .7f);
        GameManager.gameManager.currentState.EndHandle ();
    }

    public void IncreasePacing () {
        AudioView.audioView.PlaySFX (0);
        StartCoroutine (IncreasePacingWithinTime ());
    }

    IEnumerator IncreasePacingWithinTime () {
        currentSpaceship.ChangeSpeed (EnumerationHelper.SpaceshipSpeedCommands.Accelerate);
        starsView.ChangeParticleSystemSpeed (true, PACING_CHANGE_TIME);
        yield return new WaitForSeconds (PACING_CHANGE_TIME);
        GameManager.gameManager.currentState.EndHandle ();
    }

    #endregion

    public void AnimateDifficultyText (int increasedDifficultyIndex) {
        switch (increasedDifficultyIndex) {
            case -1:
                int currentTutorialWave = WaveManager.waveManager.currentWaveNumber + WaveManager.TutorialWavesNumber + 1;
                if (currentTutorialWave == WaveManager.TutorialWavesNumber) {
                    difficultyText.text = "Last Training Wave!";
                } else {
                    difficultyText.text = $"Training Wave: {currentTutorialWave}/{WaveManager.TutorialWavesNumber}!";
                }
                break;

            case 0:
                difficultyText.text = "Increased the number of obstacles!";
                break;

            case 1:
                difficultyText.text = "Decreased the obstacles visualization time!";
                break;

            case 2:
                difficultyText.text = "Decreased the time to resolve the wave!";
                break;
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate (difficultyTextRect);
        float targetXPos = (difficultyTextRect.sizeDelta.x + 75f) * -1f;
        difficultyTextRect.DOKill ();
        difficultyTextRect.DOAnchorPosX (targetXPos, PACING_CHANGE_TIME * .3f)
            .SetEase (Ease.OutExpo).SetUpdate (true).OnComplete (() => {
                difficultyTextRect.DOAnchorPosX (0f, PACING_CHANGE_TIME * .7f)
                    .SetEase (Ease.InExpo).SetUpdate (true);
            });
    }
}