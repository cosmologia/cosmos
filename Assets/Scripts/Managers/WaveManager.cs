﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager {

    public static readonly WaveManager waveManager = new WaveManager ();
    public GameObject[] WavePositionArray;
    public int currentWaveNumber;
    public Wave currentWave;

    //GAMEPLAY VALUES
    public int currentSectionNumber { get; set; }
    public float currentPatternTime { get; set; }
    public float currentSectionVisibilityTime { get; set; }
    public int lastIncreasedDifficulty;

    public Section firstSection, secondSection, thirdSection, fourthSection, fifthSection;

    bool canIncreaseSectionNumberDifficulty, canIncreasePatternTimeDifficulty, canIncreaseSectionVisibilityDifficulty;

    public const int TutorialWavesNumber = 3;

    public WaveManager () {
        SetDefaultValues ();
    }

    public void SetDefaultValues () {
        canIncreaseSectionNumberDifficulty = true;
        canIncreasePatternTimeDifficulty = true;
        canIncreaseSectionVisibilityDifficulty = true;

        const int INITIAL_SECTION_NUMBER_DIFFICULTY = 3;
        currentSectionNumber = INITIAL_SECTION_NUMBER_DIFFICULTY;

        const float INITIAL_PATTERN_TIME_DIFFICULTY = 5f;
        currentPatternTime = INITIAL_PATTERN_TIME_DIFFICULTY;

        const float INITIAL_SECTION_VISIBILITY_TIME_DIFFICULTY = 3f;
        currentSectionVisibilityTime = INITIAL_SECTION_VISIBILITY_TIME_DIFFICULTY;

        currentWaveNumber = TutorialWavesNumber * -1; //There are 3 tutorial waves
    }

    public bool IncreaseRandomDifficulty () {
        bool hasIncreasedDifficulty = false;
        do {
            const int DIFFICULTY_FACTORS = 3;
            lastIncreasedDifficulty = Random.Range (0, DIFFICULTY_FACTORS);
            switch (lastIncreasedDifficulty) {
                case 0:
                    const int MAX_SECTION_NUMBER_DIFFICULTY = 7;
                    const int SECTION_NUMBER_CHANGE_AMOUNT = 1;
                    if (currentSectionNumber < MAX_SECTION_NUMBER_DIFFICULTY) {
                        currentSectionNumber += SECTION_NUMBER_CHANGE_AMOUNT;
                        hasIncreasedDifficulty = true;
                    } else {
                        canIncreaseSectionNumberDifficulty = false;
                    }
                    break;

                case 1:
                    const float MIN_PATTERN_TIME_DIFFICULTY = 3f;
                    const float PATTERN_TIME_CHANGE_AMOUNT = .5f;
                    if (currentPatternTime > MIN_PATTERN_TIME_DIFFICULTY) {
                        currentPatternTime -= PATTERN_TIME_CHANGE_AMOUNT;
                        hasIncreasedDifficulty = true;
                    } else {
                        canIncreasePatternTimeDifficulty = false;
                    }
                    break;

                case 2:
                    const float MIN_SECTION_VISIBILITY_TIME_DIFFICULTY = 1.5f;
                    const float SECTION_VISIBILITY_TIME_CHANGE_AMOUNT = .25f;
                    if (currentSectionVisibilityTime > MIN_SECTION_VISIBILITY_TIME_DIFFICULTY) {
                        currentSectionVisibilityTime -= SECTION_VISIBILITY_TIME_CHANGE_AMOUNT;
                        hasIncreasedDifficulty = true;
                    } else {
                        canIncreaseSectionVisibilityDifficulty = false;
                    }
                    break;
            }

            if (!hasIncreasedDifficulty &&
                !canIncreaseSectionNumberDifficulty &&
                !canIncreasePatternTimeDifficulty &&
                !canIncreaseSectionVisibilityDifficulty) {
                Debug.Log (123);
                return false;
            }

        } while (hasIncreasedDifficulty == false);
        //Debug.Log (lastIncreasedDifficulty);
        return true;
    }

    public void BuildWave () {
        currentWave = new Wave (currentSectionNumber);
        const int MAX_REWARDS = 1;
        currentWave.FillSections (MAX_REWARDS);
        currentWaveNumber++;
    }

    public void CreatePredefinedSections () {
        firstSection = new Section ();
        firstSection.elementArray[1, 1] = new Common ();
        firstSection.isAnticosmosHorizontal = false;
        firstSection.isAnticosmosVertical = false;

        secondSection = new Section ();
        secondSection.isAnticosmosHorizontal = true;
        secondSection.isAnticosmosVertical = false;
        secondSection.elementArray[0, 2] = new Common ();
        secondSection.elementArray[1, 2] = new Common ();
        secondSection.elementArray[2, 2] = new Common ();

        thirdSection = new Section ();
        thirdSection.isAnticosmosVertical = true;
        thirdSection.isAnticosmosHorizontal = false;
        thirdSection.elementArray[0, 1] = new Common ();
        thirdSection.elementArray[0, 2] = new Common ();
        thirdSection.elementArray[1, 1] = new Common ();
        thirdSection.elementArray[1, 2] = new Common ();

        fourthSection = new Section ();
        fourthSection.isAnticosmosHorizontal = true;
        fourthSection.isAnticosmosVertical = false;
        fourthSection.elementArray[1, 1] = new Common ();
        fourthSection.elementArray[0, 1] = new Reward ();
        fifthSection = new Section ();
        fifthSection.isAnticosmosVertical = true;
        fifthSection.isAnticosmosHorizontal = false;
        fifthSection.elementArray[0, 1] = new Common ();
        fifthSection.elementArray[0, 2] = new Reward ();
        fifthSection.elementArray[1, 2] = new Common ();
        fifthSection.elementArray[2, 0] = new Reward ();
        fifthSection.elementArray[2, 1] = new Common ();
    }

    public void AssignSectionsToCurrentWave (Section[] sectionArray) {
        currentWave = new Wave (sectionArray.Length);
        currentWave.sectionArray = sectionArray;
    }

}