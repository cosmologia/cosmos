﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	const float TopPanelShowYPos = 0f,
		TopPanelHideYPos = 60f;

	public static UIManager uiManager;
	public GameObject ScoreUI;
	public GameObject GameplayUI;
	public GameObject PauseUI;
	public GameObject GameplayDataUI;
	public RectTransform TopPanel;
	public RectTransform bottomPanel;
	public Image overlayImage;
	public PostProcessHandler ppHandler;

	void Awake () {
		if (uiManager == null) {
			uiManager = this;
		} else {
			Destroy (gameObject);
		}
	}

	public void Start () => overlayImage.DOFade (0f, 1f).SetEase (Ease.InCubic);

	public void ChangeUIVisibility (GameObject ui, bool value) {
		Vector3 startScale = (value) ? Vector3.zero : Vector3.one;
		Vector3 endScale = (value) ? Vector3.one : Vector3.zero;
		ui.transform.localScale = startScale;
		ui.transform.DOScale (endScale, .5f).SetEase (Ease.OutQuart);
	}

	public void ToogleUIVisibility (GameObject ui) {
		if (ui.activeSelf) {
			ui.SetActive (false);
		} else {
			ui.SetActive (true);
		}
	}

	public void HidePanels () {
		TopPanel.DOKill ();
		TopPanel.DOAnchorPosY (60f, .4f).SetEase (Ease.InSine).SetUpdate (true);
		ToggleBottomPanel (false);
	}

	public void ToggleBottomPanel (bool show) {
		bottomPanel.DOKill ();
		if (show) {
			bottomPanel.DOAnchorPosY (0f, .4f).SetEase (Ease.InSine).SetUpdate (true);
		} else {
			bottomPanel.DOAnchorPosY (-110f, .4f).SetEase (Ease.InSine).SetUpdate (true);
		}
	}

	public void ToggleTopPanel (bool show) {
		TopPanel.DOKill ();
		float targetYPos = show? TopPanelShowYPos : TopPanelHideYPos;
		Ease ease = show? Ease.OutExpo : Ease.InExpo;
		TopPanel.DOAnchorPosY (targetYPos, .5f).SetEase (ease).SetUpdate (true);
	}
}