﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class TextAnimator : MonoBehaviour {

    Text textComponent;
    string text;

    public float duration = .2f;
    public Ease ease = Ease.InExpo;

    public float fadeDuration = .3f;
    public Ease fadeEase = Ease.OutCubic;

    void Awake () {
        textComponent = GetComponent<Text> ();
    }

    void OnEnable () {
        text = textComponent.text;
        textComponent.text = string.Empty;
        textComponent.SetText (text, duration, ease);

        //Do not use dokill because SetText has a dotween animation
        textComponent.color = new Color (1f, 1f, 1f, 0f);
        textComponent.DOFade (1f, fadeDuration).SetEase (fadeEase);
    }
}