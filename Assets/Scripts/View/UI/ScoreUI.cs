﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

	public Text scoreTextInfo;
	public Text scoreText;

	public void UpdateScore () {
		if (Player.player.hasNewTopScore){
			scoreTextInfo.text = "New top score!";
		} else {
			scoreTextInfo.text = "Your score";
		}
		
		scoreText.text = Player.player.currentScore.ToString();
	}

	
	public void Restart(){
		AudioView.audioView.PlaySFX (1);
		SceneManager.LoadScene("Gameplay");
		Player.player.currentScore=0;
		Player.player.currentSpaceship.numberOfShields = 3;		
		Player.player.isSpaceshipAlive = true;
		WaveManager.waveManager.SetDefaultValues();
		GameManager.gameManager.StartGame();
		SceneManager.LoadScene("Gameplay");
	}

	public void BackToMainMenu (){
		Player.player.currentScore=0;
		Player.player.currentSpaceship.numberOfShields = 3;		
		Player.player.isSpaceshipAlive = true;
		AudioView.audioView.PlaySFX (5);
		SceneManager.LoadScene ("MenuScreen");
	}
}
