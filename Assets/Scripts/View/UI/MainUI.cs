﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {

	const float TransitionLength = 3f;

	public PostProcessHandler ppHandler;
	public Image inputBlockerOverlayImage;

	[Header ("Panels")]
	public GameObject backgroundPanel;
	public GameObject mainMenuPanel;
	public GameObject creditsPanel;
	public GameObject optionsPanel;
	public GameObject transitionPanel;

	[Header ("Ship Selection")]
	public GameObject shipselection;
	public GameObject pivot;

	[Header ("Start Logo")]
	public GameObject cosmosLogo;
	public Text cosmosTitleText;
	public GameObject pressAnyText;

	[Header ("Options")]
	public Slider musicVolume;
	public Slider SFXVolume;

	[Header ("Rezisable Panel")]
	public RectTransform resizablePanel;
	public float resizablePanelAnimDuration;
	public Ease resizablePanelAnimEase;

	CanvasGroup canvasGroup;
	float startSize, finalSize;

	void Awake()
	{
		//Screen.SetResolution(1920, 1080, FullScreenMode.Windowed, 30);
	}

	void Start () {
		transitionPanel.SetActive (true);
		cosmosLogo.SetActive (true);
		mainMenuPanel.SetActive (false);
		creditsPanel.SetActive (false);
		optionsPanel.SetActive (false);
		shipselection.SetActive (false);
		pivot.SetActive (false);
		canvasGroup = transitionPanel.GetComponent<CanvasGroup> ();
		DoTransitionAnimation (0f, TransitionLength * 2f, true, true);
		AudioView.audioView.PlayMusic (0);
		musicVolume.value = AudioView.audioView.musicSource.volume;
		SFXVolume.value = AudioView.audioView.SFXSource.volume;

		cosmosTitleText.DOFade (1f, TransitionLength * 2f).SetEase (Ease.Linear);

		// chromaticAberration.intensity.value = 1f;
		// AnimateChromaticAberration (0f, TransitionLength * 2f, Ease.OutBack);
	}

	public void StartGame () {
		AudioView.audioView.PlaySFX (1);
		cosmosLogo.SetActive (false);
		ChangeContentPanelSize (new Vector3 (1.6f, 1.6f, 1f));
		mainMenuPanel.SetActive (true);
	}

	public void NewGame () {
		StartCoroutine (GoingToShipSelector ());
	}

	IEnumerator GoingToShipSelector () {
		AudioView.audioView.PlaySFX (5);
		DoTransitionAnimation (1f, TransitionLength, false);
		yield return new WaitForSecondsRealtime (TransitionLength);
		SetStandardShipSelection ();
		DoTransitionAnimation (0f, TransitionLength / 3f, true);
	}

	public void QuitGame () {
		AudioView.audioView.PlaySFX (2);
		Application.Quit ();
	}

	public void GameCredits () {
		AudioView.audioView.PlaySFX (1);
		mainMenuPanel.SetActive (false);
		creditsPanel.SetActive (true);
		ChangeContentPanelSize (new Vector3 (2.3f, 2.3f, 1f));
	}

	public void Options () {
		AudioView.audioView.PlaySFX (1);
		mainMenuPanel.SetActive (false);
		optionsPanel.SetActive (true);
		ChangeContentPanelSize (new Vector3 (2.3f, 2.3f, 1f));
	}

	public void BackToMainMenu () {
		AudioView.audioView.PlaySFX (2);
		SetStandardMainMenu ();
		ChangeContentPanelSize (new Vector3 (1.6f, 1.6f, 1f));
	}

	public void BackFromShipSelection () {
		StartCoroutine (BackFromShipSelectionCoroutine ());
	}

	IEnumerator BackFromShipSelectionCoroutine () {
		AudioView.audioView.PlaySFX (5);
		DoTransitionAnimation (1f, TransitionLength, false);
		yield return new WaitForSecondsRealtime (TransitionLength);
		SetStandardMainMenu ();
		DoTransitionAnimation (0f, TransitionLength / 3f, true);
	}

	public void SetStandardMainMenu () {
		backgroundPanel.SetActive (true);
		mainMenuPanel.SetActive (true);
		creditsPanel.SetActive (false);
		optionsPanel.SetActive (false);
		shipselection.SetActive (false);
		pivot.SetActive (false);
	}

	public void SetStandardShipSelection () {
		backgroundPanel.SetActive (false);
		mainMenuPanel.SetActive (false);
		creditsPanel.SetActive (false);
		optionsPanel.SetActive (false);
		shipselection.SetActive (true);
		pivot.SetActive (true);
	}

	public void Shut () {
		backgroundPanel.SetActive (false);
		cosmosLogo.SetActive (false);
		mainMenuPanel.SetActive (false);
		creditsPanel.SetActive (false);
		optionsPanel.SetActive (false);
		shipselection.SetActive (false);
		pivot.SetActive (false);
	}

	public void ChangeMusicVolume () {
		AudioView.audioView.musicSource.volume = musicVolume.value;
	}

	public void ChangeSFXVolume () {
		AudioView.audioView.SFXSource.volume = SFXVolume.value;
		AudioView.audioView.PlaySFX (1);
	}

	void ChangeContentPanelSize (Vector3 finalSize) {
		resizablePanel.DOScale (finalSize, resizablePanelAnimDuration)
			.SetEase (resizablePanelAnimEase)
			.OnComplete (() => {
				inputBlockerOverlayImage.raycastTarget = false;
			});
	}

	public void DoTransitionAnimation (float target, float duration, bool turningOnInput, bool firstOpeningGame = false) {
		//Chromatic Aberration
		ppHandler.AnimateChromaticAberration (target, duration, Ease.InCubic);

		//Overlay
		canvasGroup.DOFade (target, duration)
			.SetEase (Ease.Flash)
			.OnComplete (() => {
				if (turningOnInput) {
					inputBlockerOverlayImage.raycastTarget = false;
				}

				if (firstOpeningGame) {
					pressAnyText.SetActive (true);
				}
			});
	}

	public void ButtonPressedScaleAnimation (RectTransform buttonPressed) {
		inputBlockerOverlayImage.raycastTarget = true;
		buttonPressed.DOKill ();
		Vector2 startSize = buttonPressed.sizeDelta;
		Vector2 maxSize = startSize + new Vector2 (10f, 10f);
		buttonPressed.DOSizeDelta (maxSize, .2f)
			.SetEase (Ease.OutExpo)
			.OnComplete (() => {
				buttonPressed.DOSizeDelta (startSize, .2f)
					.SetEase (Ease.OutExpo);
			});
	}

	public void ButtonPressedMoveAnimation (RectTransform buttonPressed) {
		buttonPressed.DOKill ();
		Vector2 startPosition = buttonPressed.anchoredPosition;
		Vector2 targetPosition = new Vector2 (15f * buttonPressed.localScale.x, 0f);
		buttonPressed.DOPunchAnchorPos (targetPosition, .3f)
			.OnKill (() => {
				buttonPressed.anchoredPosition = startPosition;
			})
			.SetEase (Ease.OutExpo);
	}

}