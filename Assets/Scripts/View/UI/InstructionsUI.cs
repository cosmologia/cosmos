﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InstructionsUI : MonoBehaviour {

	readonly string[] Instructions = {
		"Each red symbol represents an obstacle in that quadrant. Avoid them at all costs.",
		"The ship will follow the pattern you trace for the flight automatically.",
		"You will have a short amount of time to trace the pattern your spaceship will go through.",
		"Movements are limited to the quadrants immediately next to the last one selected.",
		"When the screen goes negative, the quadrants will be inversely turned. This is the Anticosmos.",
		"Look for an horizontal or vertical turn by looking at the white background of the screen.",
		"Force shields will protect you in case you made a mistake. Earn points to get more shields.",
		"Along your flight you will encounter some rewards. Grab them to earn extra points."
	};

	public PostProcessHandler ppHandler;
	public Image[] instructionImages;
	public GameObject playButton;
	public Text instructionText;
	public Button forwardButton;
	public Button backButton;
	public CanvasGroup inputBlockerOverlayImage;
	int currentInstructionIndex;

	void Start () {
		currentInstructionIndex = 1;
		ShowInstructions (-1);

		ppHandler.AnimateChromaticAberration (0f, 1.5f, Ease.OutCubic);

		inputBlockerOverlayImage.DOFade (0f, 1.5f).SetEase (Ease.OutFlash)
			.OnComplete (() => inputBlockerOverlayImage.blocksRaycasts = false);
	}

	public void Play () { // Called by scene button
		AudioView.audioView.PlaySFX (5);
		ppHandler.AnimateChromaticAberration (1f, 2f, Ease.OutCubic);
		inputBlockerOverlayImage.blocksRaycasts = true;
		inputBlockerOverlayImage.DOKill ();
		inputBlockerOverlayImage.DOFade (1f, 2f)
			.SetEase (Ease.InFlash).OnComplete (() => SceneManager.LoadScene ("Gameplay"));
	}

	public void ShowInstructions (int direction) {
		inputBlockerOverlayImage.blocksRaycasts = true;

		int lastInstructionsIndex = currentInstructionIndex;
		currentInstructionIndex += direction;

		if (currentInstructionIndex < 0)
			currentInstructionIndex = 0;
		else if (currentInstructionIndex > instructionImages.Length - 1)
			currentInstructionIndex = instructionImages.Length - 1;

		if (lastInstructionsIndex != currentInstructionIndex) {
			const float InstructionImageFadeDuration = .2f;

			instructionImages[lastInstructionsIndex].DOKill ();
			instructionImages[lastInstructionsIndex].DOFade (0f, InstructionImageFadeDuration).SetEase (Ease.InQuint)
				.OnComplete (() => {
					inputBlockerOverlayImage.blocksRaycasts = false;
					instructionImages[lastInstructionsIndex].gameObject.SetActive (false);
				});

			instructionImages[currentInstructionIndex].gameObject.SetActive (true);
			instructionImages[currentInstructionIndex].DOKill ();
			instructionImages[currentInstructionIndex].DOFade (1f, InstructionImageFadeDuration).SetEase (Ease.OutQuint);

			instructionText.text = Instructions[currentInstructionIndex];
		} else {
			inputBlockerOverlayImage.blocksRaycasts = false;
		}

		bool showBackButton = currentInstructionIndex > 0;
		backButton.gameObject.SetActive (showBackButton);

		bool showForwardButton = currentInstructionIndex < instructionImages.Length - 1;
		forwardButton.gameObject.SetActive (showForwardButton);
	}

}