﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraRotation : MonoBehaviour/*, IBeginDragHandler, IDragHandler, IEndDragHandler */{
	public Transform pivot; 
	public Transform myCamera;

	private Vector3 startTouchPos;
	private Vector3 endTouchPos;

	private Vector3 firstTouch;
	private Vector3 endTouch;

	private float speedX;

//    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData){ 
//		if (pivot.gameObject.activeSelf){
//			startTouchPos = Input.mousePosition;
//		}
//
//    }
//
//    void IDragHandler.OnDrag(PointerEventData eventData){
//		
//    }
//
//    void IEndDragHandler.OnEndDrag(PointerEventData eventData){
//		if (pivot.gameObject.activeSelf){
//			endTouchPos = Input.mousePosition;
//			RotateCamera(startTouchPos,endTouchPos);
//		}
//    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		
		myCamera.LookAt(pivot.position);


		if (Input.touchCount <= 0)
			return; 
		if (Input.GetTouch (0).phase == TouchPhase.Began) {
			firstTouch = Input.GetTouch (0).position;
		}

		if (Input.GetTouch (0).phase == TouchPhase.Ended) {
			endTouch = Input.GetTouch (0).position;
			SetPositions (firstTouch, endTouch);
		}
			
		myCamera.LookAt(pivot.position);

		if (speedX>=0)
			speedX-=Time.deltaTime;	
		 else 
			speedX+=Time.deltaTime;
		
		pivot.Rotate(new Vector3 (0,speedX*Time.deltaTime*-1,0));

	}
	
	void RotateCamera (Vector3 first, Vector3 end){
		speedX = (first.x -end.x);

	} 

	void SetPositions (Vector3 first, Vector3 end) {
		RotateCamera (first, end);
	}
	


}
