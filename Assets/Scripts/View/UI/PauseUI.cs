﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour {

	public bool isPaused;
	public Button pauseButton;
	public Sprite playSprite, playDisabledSprite, pauseSprite;
	CanvasGroup canvasGroup;

	void Start () {
		isPaused = false;
		canvasGroup = GetComponent<CanvasGroup> ();
		pauseButton.image.sprite = pauseSprite;
		AudioView.audioView.PlayMusic (1);
	}

	public void Pause () {
		if (!isPaused) {
			isPaused = true;
			pauseButton.image.sprite = playDisabledSprite;
			AudioView.audioView.PlaySFX (1);
			Time.timeScale = 0f;
			DOTween.PauseAll ();
			canvasGroup.blocksRaycasts = true;
			pauseButton.interactable = false;
			canvasGroup.DOFade (1f, 1f).SetEase (Ease.OutExpo).SetUpdate (true).OnComplete (() => {
				canvasGroup.interactable = true;
				pauseButton.interactable = true;
				pauseButton.image.sprite = playSprite;
			});
		} else {
			DOTween.PlayAll ();
			isPaused = false;
			pauseButton.image.sprite = pauseSprite;
			AudioView.audioView.PlaySFX (2);
			string currentStateName = GameManager.gameManager.currentState.GetType ().ToString ();
			// if (currentStateName == "PatternInputState" || currentStateName == "WaveVisualizationState") {
			// 	WaveManager.waveManager.currentWaveNumber--;
			// 	SceneManager.LoadScene ("Gameplay");
			// }
			if (currentStateName == "PatternResolutionState") {
				Time.timeScale = ((SectionView3D) GamePlayViewManager.gamePlayViewManager.section3D).currentTimeScale;
			} else {
				Time.timeScale = 1f;
			}

			canvasGroup.blocksRaycasts = false;
			canvasGroup.DOFade (0f, 1f).SetEase (Ease.OutExpo).SetUpdate (true).OnComplete (() => {
				canvasGroup.interactable = false;
			});
		}
	}

	public void Restart () {
		Time.timeScale = 1f;
		AudioView.audioView.PlaySFX (2);
		Player.player.isSpaceshipAlive = true;
		Player.player.currentScore = 0;
		Player.player.currentSpaceship.numberOfShields = 3;
		Player.player.hasNewTopScore = false;
		WaveManager.waveManager.SetDefaultValues ();
		GameManager.gameManager.StartGame ();
		SceneManager.LoadScene ("Gameplay");
	}

	public void Quit () {
		Player.player.currentScore = 0;
		Player.player.currentSpaceship.numberOfShields = 3;
		Player.player.isSpaceshipAlive = true;
		Player.player.hasNewTopScore = false;
		Time.timeScale = 1f;
		AudioView.audioView.PlaySFX (2);
		SceneManager.LoadScene ("MenuScreen");
	}
}