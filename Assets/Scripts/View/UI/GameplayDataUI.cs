﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameplayDataUI : MonoBehaviour {

	public Text score;
	public Text remainingTime;
	public Text crossedWaves;
	public Text shield;
	public RectTransform shieldRect;

	public GameObject scorePanel;
	public GameObject timePanel;
	public GameObject sectionIcon;
	public GameObject sectionPanel;
	public RectTransform sectionArrow;
	public GameObject iconStorage;
	public RectTransform wavesPanel, shieldsPanel;

	public int currentSectionNumber;

	public List<RectTransform> Icons;

	int lastWaveNumber;
	int lastShieldNumber;
	int lastScoreNumber;

	void Start () {
		scorePanel.SetActive (false);
		timePanel.SetActive (false);
		sectionArrow.gameObject.SetActive (false);
		lastWaveNumber = -1;
		lastShieldNumber = -1;
		lastScoreNumber = -1;
	}

	void Update () {
		ShowScore ();
		ShowShieldNumber ();
		ShowNumberOfCrossedWaves ();
	}

	public void ShowSidePanels () {
		wavesPanel.gameObject.SetActive (true);
		wavesPanel.DOAnchorPosX (-2f, 1f).SetEase (Ease.OutExpo).SetUpdate (true);

		shieldsPanel.gameObject.SetActive (true);
		shieldsPanel.DOAnchorPosX (2f, 1f).SetEase (Ease.OutExpo).SetUpdate (true);
	}

	public void ShowScoreOnBottomPanel (bool showScore) {
		scorePanel.SetActive (showScore);

		sectionArrow.gameObject.SetActive (!showScore);
		timePanel.SetActive (!showScore);

		if (showScore == true)
			HideIcons ();
	}

	public void ShowScore () {
		if (lastScoreNumber == Player.player.currentScore) return;

		lastScoreNumber = Player.player.currentScore;
		score.text = Player.player.currentScore.ToString ();
		ShowTextFeedback (score, new Vector2 (100f, 50f), new Vector2 (100f, 60f), .5f);
	}

	public void ShowRemainingTime (float t) {
		StartCoroutine (TimerDown (t));
		remainingTime.color = Color.white;
		remainingTime.DOKill ();
		remainingTime.DOColor (Color.red, t).SetEase (Ease.Linear);
	}

	public void ShowNumberofSections (int currentNumberOfSections) {
		for (int i = 0; i < currentNumberOfSections; i++) {
			Icons[i].SetParent (sectionPanel.transform);
			Icons[i].anchoredPosition = new Vector3 (0, 0, 0);
			Icons[i].localScale = Vector3.zero;
			Icons[i].DOScale (Vector3.one, .5f).SetEase (Ease.OutBack);
		}
	}

	public void ShowCurrentSection (int currentSection) {
		Canvas.ForceUpdateCanvases ();
		RectTransform sectionArrowRect = sectionArrow.GetComponent<RectTransform> ();
		Vector3 targetPosition = new Vector3 (Icons[currentSection].anchoredPosition.x, sectionArrowRect.anchoredPosition.y, 0f);
		sectionArrowRect.DOKill ();
		if (currentSection > 0) {
			sectionArrowRect.DOAnchorPosX (targetPosition.x, .2f).SetEase (Ease.OutExpo);
		} else {
			sectionArrowRect.anchoredPosition = targetPosition;
			sectionArrow.transform.localScale = Vector3.zero;
			sectionArrow.transform.DOScale (Vector3.one, .4f).SetEase (Ease.OutExpo);
		}
	}

	public void ShowNumberOfCrossedWaves () {
		if (lastWaveNumber == WaveManager.waveManager.currentWaveNumber) return;

		lastWaveNumber = WaveManager.waveManager.currentWaveNumber;
		if (WaveManager.waveManager.currentWaveNumber < 0) {
			crossedWaves.text = "0";
		} else {
			crossedWaves.text = WaveManager.waveManager.currentWaveNumber.ToString ();
			ShowTextFeedback (crossedWaves, new Vector2 (45f, 45f), new Vector2 (70f, 70f), 1f);
		}
	}

	public void ShowShieldNumber () {
		if (lastShieldNumber == Player.player.currentSpaceship.numberOfShields) return;

		lastShieldNumber = Player.player.currentSpaceship.numberOfShields;
		shield.text = Player.player.currentSpaceship.numberOfShields.ToString ();

		shieldRect.DOShakePosition (1f, 20f, 20).SetUpdate (true);
	}

	void ShowTextFeedback (Text text, Vector2 startSize, Vector2 endSize, float duration) {
		RectTransform crossedWavesRect = text.GetComponent<RectTransform> ();
		crossedWavesRect.DOSizeDelta (endSize, duration / 2f)
			.SetEase (Ease.OutQuint).SetUpdate (true)
			.OnComplete (() => {
				crossedWavesRect.DOSizeDelta (startSize, duration / 2f)
					.SetUpdate (true).SetEase (Ease.InQuint);
			});
	}

	public void FormatTime (float t) {
		string seconds = (t % 60f).ToString ("f2");
		remainingTime.text = seconds;
	}

	public void MoveSectionArrow () {
		currentSectionNumber++;
		ShowCurrentSection (currentSectionNumber);
	}

	public void ResetSectionArrow () {
		currentSectionNumber = 0;
		ShowCurrentSection (currentSectionNumber);
	}

	public void HideIcons () {
		for (int i = 0; i < WaveManager.waveManager.currentWave.sectionArray.Length; i++) {
			Icons[i].SetParent (iconStorage.transform);
			Icons[i].anchoredPosition = new Vector3 (0, 0, 0);
		}
	}

	IEnumerator TimerDown (float t) {
		while (t > 0f) {
			FormatTime (t);
			t -= Time.deltaTime;
			yield return null;
		}
		yield return null;
	}
}