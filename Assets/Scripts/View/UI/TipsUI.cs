﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TipsUI : MonoBehaviour {

	public Text currentTip;
	public Slider slider1;
	public Slider slider2;

	int tipID;
	int maxTipID;
	float progress;

	string[] TipArray = new string[3] {
		"Remember, you can only move to an adjacent space.",
		"Take account of the number of sections before making a move.",
		"Difficulty increases every time you complete a wave." } ;

	private AsyncOperation async;

	void Start () {
		maxTipID = TipArray.Length;
		ShowRandomTip ();
		StartCoroutine (LoadScene ("Gameplay"));
	}

	void ShowRandomTip (){
		tipID = Random.Range (0, maxTipID);
		currentTip.text = TipArray[tipID];
	}

	IEnumerator LoadScene(string levelName){
		async = SceneManager.LoadSceneAsync (levelName);
		while (async.isDone == false) {
			progress = Mathf.Clamp01 (async.progress / .9f);
			slider1.value = progress;
			slider2.value = progress;
			yield return null;
		}
	}
}
