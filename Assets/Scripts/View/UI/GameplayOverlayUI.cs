﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameplayOverlayUI : MonoBehaviour {

    public Image overlayImage;

    void Start () {
        overlayImage.DOFade (0f, 1f).SetEase (Ease.InCubic);
    }

}