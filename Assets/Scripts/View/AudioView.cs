﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioView : MonoBehaviour {

	public static AudioView audioView;
	public AudioClip[] SFX = new AudioClip[6];
	public AudioClip[] Ambiance = new AudioClip[2];
	public AudioClip[] Music = new AudioClip[2];
	public AudioSource musicSource;
	public AudioSource SFXSource;
	public AudioSource ambianceSource;

	void Awake() {
		DontDestroyOnLoad (this.gameObject);
		if (audioView == null) {
			audioView = this;
		} else {
			Destroy(gameObject);
		}
	}

	public void PlaySFX(int index){
		SFXSource.clip = SFX[index];
		SFXSource.PlayOneShot(SFX [index]);
	}

	public void PlayAmbiance(int index){
		ambianceSource.clip = Ambiance[index];
		ambianceSource.Play();
	}

	public void PlayMusic(int index){
		musicSource.clip = Music[index];
		musicSource.Play();
	}
		
}
