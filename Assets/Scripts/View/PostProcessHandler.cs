﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessHandler : MonoBehaviour {

    ChromaticAberration chromaticAberration;
    TweenerCore<float, float, FloatOptions> chromaticAberrationIntensityTween;

    const float DefaultLensDistortionIntensity = 6f;
    LensDistortion lensDistortion;
    TweenerCore<Vector3, Vector3[], Vector3ArrayOptions> lensDistortionIntensityTween;
    TweenerCore<float, float, FloatOptions> lensDistortionIntensityTweenFloat;

    void Awake () {
        Camera camera = GetComponent<Camera> ();

        PostProcessVolume ppVolume = camera.GetComponent<PostProcessVolume> ();
        ppVolume.profile.TryGetSettings (out chromaticAberration);
        ppVolume.profile.TryGetSettings (out lensDistortion);
    }

    #region Chromatic Aberration
    public float ChromaticAberrationIntensity {
        get { return chromaticAberration.intensity; }
        set { chromaticAberration.intensity.value = value; }
    }

    public void AnimateChromaticAberration (float target, float duration, Ease ease) {
        if (chromaticAberrationIntensityTween != null)
            chromaticAberrationIntensityTween.Kill ();

        chromaticAberrationIntensityTween = DOTween.To (
            () => ChromaticAberrationIntensity,
            x => ChromaticAberrationIntensity = x,
            target, duration).SetEase (ease);
    }
    #endregion

    #region Lens Distortion
    public Vector3 LensDistortionIntensity {
        get {
            return Vector3.one * lensDistortion.intensity;
        }
        set {
            lensDistortion.intensity.value = value.x;
        }
    }

    public float LensDistortionIntensityFloat {
        get {
            return lensDistortion.intensity;
        }
        set {
            lensDistortion.intensity.value = value;
        }
    }

    public void AnimateLensDistortion (int vibrato, float elasticity, float punchDuration, Ease punchEase,
        float shakeDuration, float shakeStrength, Ease shakeEase) {
        if (lensDistortionIntensityTween != null)
            lensDistortionIntensityTween.Kill ();

        lensDistortionIntensityTween = DOTween.Punch (
            () => LensDistortionIntensity,
            x => LensDistortionIntensity = x,
            Vector3.one * 80, punchDuration, vibrato, elasticity).SetEase (Ease.Linear).OnComplete (() => {
            lensDistortionIntensityTween = DOTween.Shake (
                () => LensDistortionIntensity,
                x => LensDistortionIntensity = x,
                shakeDuration, shakeStrength).SetEase (shakeEase);
        });
    }

    public void StopLensDistortionAnimation (float duration, Ease ease) {
        if (lensDistortionIntensityTween != null)
            lensDistortionIntensityTween.Kill ();

        lensDistortionIntensityTweenFloat = DOTween.To (
            () => LensDistortionIntensityFloat,
            x => LensDistortionIntensityFloat = x,
            DefaultLensDistortionIntensity, duration).SetEase (ease);
    }
    #endregion

}