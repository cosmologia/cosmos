﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnticosmosView : MonoBehaviour {

	public Material AnticosmosCameraFilter;
	float materialColorChangeDuration = .4f;

	void Awake() {
        AnticosmosCameraFilter.color = new Color (0,0,0,0);
	}

	public void ChangeNegativeFilterGradually (bool isTurningOn){
		if (isTurningOn){
            StartCoroutine(ChangeMaterialColorGradually(0f, 1f, materialColorChangeDuration));
        } else {
            StartCoroutine(ChangeMaterialColorGradually(1f, 0f, materialColorChangeDuration));
        }
	}

    IEnumerator ChangeMaterialColorGradually(float from, float to, float transitionDuration){
        Color tempAnticosmosCameraFilterColor = new Color();
        float currentLerpTime = 0f, percentage = 0f;
        do {
            currentLerpTime += Time.deltaTime;
            percentage = currentLerpTime / transitionDuration;
            tempAnticosmosCameraFilterColor.r = Mathf.Lerp(from, to, percentage);
            tempAnticosmosCameraFilterColor.g = tempAnticosmosCameraFilterColor.r;
            tempAnticosmosCameraFilterColor.b = tempAnticosmosCameraFilterColor.r;
            tempAnticosmosCameraFilterColor.a = tempAnticosmosCameraFilterColor.r;
            AnticosmosCameraFilter.color = tempAnticosmosCameraFilterColor;
            yield return new WaitForEndOfFrame();
        } while (tempAnticosmosCameraFilterColor.a != to);
    }
}
