﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShieldView : MonoBehaviour {

	Transform alphaModel;

	void Awake(){
		alphaModel = transform.GetChild(0);
	}
	
	public void Show(){
		alphaModel.DOKill();
		transform.DOKill();
		transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), .3f).SetEase(Ease.OutQuint).SetUpdate(true);
		alphaModel.DOScale(Vector3.one, .3f).SetEase(Ease.OutQuint).SetUpdate(true).SetDelay(.1f);
	}

	void OnTriggerEnter(Collider obj){
		if (obj.CompareTag("Obstacle")){
			obj.GetComponent<ISectionElementView3D>().Destroy();
			Dissipate();
		}
	}
	
	public void Dissipate(){
		alphaModel.DOKill();
		transform.DOKill();
		alphaModel.DOScale(Vector3.zero, .3f).SetEase(Ease.OutQuint).SetUpdate(true);
		transform.DOScale(Vector3.zero, .3f).SetEase(Ease.OutQuint).SetUpdate(true).SetDelay(.1f);
	}
}
