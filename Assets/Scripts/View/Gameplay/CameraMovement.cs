﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	readonly string[] topLeftAnimationArray = new string[2] { "TopLeft", "CenterLeft" };
	readonly string[] topAnimationArray = new string[6] { "TopLeft", "Top", "TopRight", "CenterLeft", "Center", "CenterRight" };
	readonly string[] topRighttAnimationArray = new string[2] { "TopRight", "CenterRight" };

	readonly string[] centerLeftAnimationArray = new string[6] { "TopLeft", "Top", "CenterLeft", "Center", "BottomLeft", "Bottom" };
	readonly string[] centerAnimationArray = new string[9] { "TopLeft", "Top", "TopRight", "CenterLeft", "Center", "CenterRight", "BottomLeft", "Bottom", "BottomRight" }; //
	readonly string[] centerRightAnimationArray = new string[6] { "Top", "TopRight", "Center", "CenterRight", "Bottom", "BottomRight" };

	readonly string[] bottomLeftAnimationArray = new string[2] { "CenterLeft", "BottomLeft" };
	readonly string[] bottomAnimationArray = new string[6] { "CenterLeft", "Center", "CenterRight", "BottomLeft", "Bottom", "BottomRight" };
	readonly string[] bottomRightAnimationArray = new string[2] { "CenterRight", "BottomRight" };

	Animator animator;

	void Start(){
		animator = GetComponent<Animator>();
	}

	public void SelectAnimationAccordingPosition (EnumerationHelper.SectionPlace place) {
		switch(place){
			case EnumerationHelper.SectionPlace.TopLeft:
				animator.SetTrigger(topLeftAnimationArray[Random.Range(0, topLeftAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.Top:
				animator.SetTrigger(topAnimationArray[Random.Range(0, topAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.TopRight:
				animator.SetTrigger(topRighttAnimationArray[Random.Range(0, topRighttAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.CenterLeft:
				animator.SetTrigger(centerLeftAnimationArray[Random.Range(0, centerLeftAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.Center:
				animator.SetTrigger(centerAnimationArray[Random.Range(0, centerAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.CenterRight:
				animator.SetTrigger(centerRightAnimationArray[Random.Range(0, centerRightAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.BottomLeft:
				animator.SetTrigger(bottomLeftAnimationArray[Random.Range(0, bottomLeftAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.Bottom:
				animator.SetTrigger(bottomAnimationArray[Random.Range(0, topLeftAnimationArray.Length)]);
				break;
			case EnumerationHelper.SectionPlace.BottomRight:
				animator.SetTrigger(bottomRightAnimationArray[Random.Range(0, topLeftAnimationArray.Length)]);
				break;
		}
	}

	public void ChangeToDefaultPosition(){
		animator.SetTrigger("Top");
	}

}
