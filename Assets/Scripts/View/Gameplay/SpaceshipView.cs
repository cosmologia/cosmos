﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipView : MonoBehaviour{

    public GameObject[] spaceshipPrefabArray;
    public GameObject shield;
    public static bool hasCollided { get; set; }
    Animator modelAC;
    Animator movementAC;

    void Awake(){
        SpawnSpaceship();
        movementAC = GetComponent<Animator>();
    }

	void SpawnSpaceship () {
        for (int i = 0; i<Spaceship.spaceshipArray.Length; i++){
            if (Spaceship.spaceshipArray[i] == Player.player.currentSpaceship.name) {
                spaceshipPrefabArray[i].gameObject.SetActive(true);
                modelAC = spaceshipPrefabArray[i].GetComponent<Animator>();
            }
        }
    }

    public void Move (EnumerationHelper.SectionPlace place){
        movementAC.SetTrigger(place.ToString());
    }

    public void ChangeSpeed(EnumerationHelper.SpaceshipSpeedCommands command){
        modelAC.SetTrigger(command.ToString());
    }

    public void ActivateShield(){
        Player.player.currentSpaceship.UseShield();
        shield.GetComponent<ShieldView>().Show();
    }

    public void DeactivateShield(){
        shield.GetComponent<ShieldView>().Dissipate();
    }

    public void Destroy(){
        gameObject.SetActive(false);
    }

	void OnTriggerEnter(Collider obj){
		if (obj.CompareTag("SectionCollider")){
			hasCollided = true;
		}
	}
}
