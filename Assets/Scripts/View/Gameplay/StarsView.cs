﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class StarsView : MonoBehaviour {

	const float LowestSpeed = .5f;
	const float HighestSpeed = 10f;
	ParticleSystem.MainModule starsPSMain;
	TweenerCore<float, float, FloatOptions> starsSimulationSpeedTween;
	public bool simulatingFast;

	public float StarsSimulationSpeed {
		get { return starsPSMain.simulationSpeed; }
		set {
			starsPSMain.simulationSpeed = value;
		}
	}

	void Awake () {
		ParticleSystem starsPS = GetComponent<ParticleSystem> ();
		starsPS.Play (false);
		starsPSMain = starsPS.main;
	}

	public void ChangeParticleSystemSpeed (bool increasePacing, float duration) {
		simulatingFast = increasePacing;

		if (starsSimulationSpeedTween != null)
			starsSimulationSpeedTween.Kill ();

		float target = increasePacing? HighestSpeed : LowestSpeed;

		starsSimulationSpeedTween = DOTween.To (
			() => StarsSimulationSpeed,
			x => StarsSimulationSpeed = x,
			target, duration).SetEase (Ease.OutCubic);
	}
}