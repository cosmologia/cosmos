﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SectionView2D : SectionView {

    const float HighestVisibility = 1f;
    const float LowestVisibility = 0f;
    const float TransitionDurationPercentage = .025f;

    public GameObject horizontalAnticosmos;
    public GameObject verticalAnticosmos;
    public Transform[] anticosmosObstaclesArray;
    public Transform anticosmosReward;

    Color currentSectionElementsColor;

    public override void HideSectionElements () {
        ChangeVisibilityGradually (HighestVisibility, LowestVisibility,
            WaveManager.waveManager.currentSectionVisibilityTime * TransitionDurationPercentage);
    }

    public void SetDefaultValues () {
        gameObject.SetActive (true);
        horizontalAnticosmos.transform.localScale = new Vector3 (0f, 1f);
        verticalAnticosmos.transform.localScale = new Vector3 (1f, 0f);
    }

    public override void ShowSectionElements (Section section) {
        horizontalAnticosmos.transform.DOKill ();
        horizontalAnticosmos.GetComponent<Image> ().DOKill ();
        verticalAnticosmos.transform.DOKill ();
        verticalAnticosmos.GetComponent<Image> ().DOKill ();
        if (section.isAnticosmosHorizontal || section.isAnticosmosVertical) {
            LocateAnticosmosSectionElements (section);
            if (section.isAnticosmosHorizontal) {
                //Debug.Log("isAnticosmosHorizontal");
                horizontalAnticosmos.GetComponent<Image> ().DOFade (1f, .4f).SetEase (Ease.OutBack);
                horizontalAnticosmos.transform.DOScaleX (1f, .4f).SetEase (Ease.OutBack);
                verticalAnticosmos.transform.DOScaleY (0f, .3f).SetEase (Ease.OutExpo);
                verticalAnticosmos.GetComponent<Image> ().DOFade (0f, .3f).SetEase (Ease.OutExpo);
            } else {
                //Debug.Log("isAnticosmosVertical");
                verticalAnticosmos.transform.DOScaleY (1f, .4f).SetEase (Ease.OutBack);
                verticalAnticosmos.GetComponent<Image> ().DOFade (1f, .4f).SetEase (Ease.OutBack);
                horizontalAnticosmos.transform.DOScaleX (0f, .3f).SetEase (Ease.OutExpo);
                horizontalAnticosmos.GetComponent<Image> ().DOFade (0f, .3f).SetEase (Ease.OutExpo);
            }
        } else {
            //Debug.Log("Normal");
            LocateSectionElements (section);
            horizontalAnticosmos.transform.DOScaleX (0f, .3f).SetEase (Ease.OutExpo);
            horizontalAnticosmos.GetComponent<Image> ().DOFade (0f, .3f).SetEase (Ease.OutExpo);
            verticalAnticosmos.transform.DOScaleY (0f, .3f).SetEase (Ease.OutExpo);
            verticalAnticosmos.GetComponent<Image> ().DOFade (0f, .3f).SetEase (Ease.OutExpo);
        }
        ResetSectionElementsPosition ();
        currentSectionElementsColor = currentSectionElementList[0].GetComponent<Image> ().color;
        ChangeVisibilityGradually (LowestVisibility, HighestVisibility,
            WaveManager.waveManager.currentSectionVisibilityTime * TransitionDurationPercentage);
    }

    void ChangeVisibilityGradually (float from, float to, float transitionDuration) {
        //Debug.Log(transitionDuration);
        for (int i = 0; i < currentSectionElementList.Count; i++) {
            currentSectionElementList[i].transform.localScale = Vector3.one;
            currentSectionElementList[i].GetComponent<ISectionElementView2D> ().ChangeColorAlpha (from, to, transitionDuration * 10f);
        }
    }

    void ResetSectionElementsPosition () {
        for (int i = 0; i < currentSectionElementList.Count; i++) {
            currentSectionElementList[i].GetComponent<RectTransform> ().localPosition = Vector3.zero;
        }
    }

    void LocateAnticosmosSectionElements (Section section) {
        int obstaclesArrayIteratorTemp = 0;
        currentSectionElementList = new List<GameObject> ();
        for (int i = 0; i < section.elementArray.GetLength (0); i++) {
            for (int j = 0; j < section.elementArray.GetLength (1); j++) {
                if (section.elementArray[i, j] != null) {
                    if (section.elementArray[i, j].tag == EnumerationHelper.SectionElementTag.Common) {
                        anticosmosObstaclesArray[obstaclesArrayIteratorTemp].SetParent (sectionElementPlaceArray[i, j]);
                        currentSectionElementList.Add (anticosmosObstaclesArray[obstaclesArrayIteratorTemp].gameObject);
                        obstaclesArrayIteratorTemp++;
                    } else if (section.elementArray[i, j].tag == EnumerationHelper.SectionElementTag.Reward) {
                        anticosmosReward.GetComponent<IRewardView> ().ShowOrbits (((Reward) section.elementArray[i, j]).multiplier);
                        anticosmosReward.SetParent (sectionElementPlaceArray[i, j]);
                        currentSectionElementList.Add (anticosmosReward.gameObject);
                    }
                }
            }
        }
    }
}