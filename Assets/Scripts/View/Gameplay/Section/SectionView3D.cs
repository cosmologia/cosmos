﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class SectionView3D : SectionView {

    const float LOWEST_TIMESCALE = .05f;
    const float HIGHEST_TIMESCALE = 1f;
    const float TIMESCALE_TRANSITION_DURATION = .3f;
    public Transform sectionElements3DStorageTransform;
    public GameObject horizontalAnticosmosPS;
    public GameObject verticalAnticosmosPS;
    public float currentTimeScale;
    public bool showing3DSection;
    public bool hiding3DSection;
    Animator animatorComponent;

    float TimeScale {
        get { return Time.timeScale; }
        set { Time.timeScale = value; }
    }

    TweenerCore<float, float, FloatOptions> timeScaleTween;

    void Awake () {
        ConvertTo2DArray ();
        animatorComponent = GetComponent<Animator> ();
    }

    public override void HideSectionElements () {
        hiding3DSection = true;
        if (timeScaleTween != null)
            timeScaleTween.Kill ();
        timeScaleTween = DOTween.To (() => TimeScale, x => TimeScale = x, HIGHEST_TIMESCALE, TIMESCALE_TRANSITION_DURATION)
            .SetEase (Ease.OutCubic).OnUpdate (() => currentTimeScale = Time.timeScale).OnComplete (() => EvaluateTimescaleChangeResult (HIGHEST_TIMESCALE));
    }

    public override void ShowSectionElements (Section section) {
        showing3DSection = true;
        if (section.isAnticosmosHorizontal) {
            horizontalAnticosmosPS.SetActive (true);
        } else if (section.isAnticosmosVertical) {
            verticalAnticosmosPS.SetActive (true);
        } else {
            verticalAnticosmosPS.SetActive (false);
            horizontalAnticosmosPS.SetActive (false);
        }
        LocateSectionElements (section);
        ResetSectionElementsPosition ();
        animatorComponent.SetTrigger ("Move");

        if (timeScaleTween != null)
            timeScaleTween.Kill ();
        timeScaleTween = DOTween.To (() => TimeScale, x => TimeScale = x, LOWEST_TIMESCALE, TIMESCALE_TRANSITION_DURATION)
            .SetEase (Ease.OutCubic).SetDelay (.45f).OnUpdate (() => currentTimeScale = Time.timeScale).OnComplete (() => EvaluateTimescaleChangeResult (LOWEST_TIMESCALE));
    }

    void EvaluateTimescaleChangeResult (float timescale) {
        if (timescale == HIGHEST_TIMESCALE) {
            if (horizontalAnticosmosPS.activeSelf) {
                horizontalAnticosmosPS.SetActive (false);
            } else if (verticalAnticosmosPS.activeSelf) {
                verticalAnticosmosPS.SetActive (false);
            }
            HideCurrentSectionElements ();
            hiding3DSection = false;
        } else {
            showing3DSection = false;
        }
    }

    void HideCurrentSectionElements () {
        for (int i = 0; i < currentSectionElementList.Count; i++) {
            currentSectionElementList[i].transform.SetParent (sectionElements3DStorageTransform);
        }
    }

    void ResetSectionElementsPosition () {
        for (int i = 0; i < currentSectionElementList.Count; i++) {
            currentSectionElementList[i].GetComponent<Transform> ().localPosition = Vector3.zero;
        }
    }
}