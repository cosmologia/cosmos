﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRewardView {

    void ShowOrbits(int multiplier);
}
