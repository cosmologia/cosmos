﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RewardView3D : MonoBehaviour, ISectionElementView3D, IRewardView {

	public GameObject[] orbits = new GameObject[3];

	public void ShowOrbits(int multiplier) {
		switch (multiplier) {
		case 1:
			orbits[0].SetActive(true);
			orbits[1].SetActive(false);
			orbits[2].SetActive(false);
			break;
		case 2:
			orbits[0].SetActive(true);
			orbits[1].SetActive(true);
			orbits[2].SetActive(false);
			break;
		case 3:
			orbits[0].SetActive(true);
			orbits[1].SetActive(true);
			orbits[2].SetActive(true);
			break;
		default:
			orbits[0].SetActive(false);
			orbits[1].SetActive(false);
			orbits[2].SetActive(false);
			break;
		}
	}

    public void ChangeVisibility(bool value){
        gameObject.SetActive(value);
    }
	
	void OnTriggerEnter(Collider obj){
		if (obj.CompareTag("Spaceship")){
			AudioView.audioView.PlaySFX (1);
			Destroy();
		}
	}
	
	public void Destroy(){
		transform.DOScale(Vector3.zero, .2f).SetEase(Ease.InBack).SetUpdate(true);
	}
	
	public void OnEnable(){
		transform.localScale = new Vector3(4f, 4f, 4f);
	}
}
