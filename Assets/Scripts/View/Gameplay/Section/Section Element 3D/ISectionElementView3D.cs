﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISectionElementView3D {

    void ChangeVisibility(bool value);
    
    void Destroy();
}
