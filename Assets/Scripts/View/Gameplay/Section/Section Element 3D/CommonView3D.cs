﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonView3D : MonoBehaviour, ISectionElementView3D{
	
    public void ChangeVisibility(bool value){
        gameObject.SetActive(value);
    }
    
    public void Destroy(){
        gameObject.SetActive(false);
    }
    
    void OnTriggerEnter(Collider obj){
		if (obj.CompareTag("Spaceship")){
			AudioView.audioView.PlaySFX (4);
            SpaceshipView.hasCollided = true;
            GamePlayViewManager.gamePlayViewManager.currentSpaceship.Destroy();
		}
	}
}
