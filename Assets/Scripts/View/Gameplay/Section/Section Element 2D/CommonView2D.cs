﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CommonView2D : MonoBehaviour, ISectionElementView2D{

    Image imageComponent;

    void Awake() {
        imageComponent = GetComponent<Image>();
    }
	
    public void ChangeColorAlpha(float startAlpha, float targetAlpha, float duration){
        Color tempColor = imageComponent.color;
		tempColor.a = startAlpha;
        imageComponent.color = tempColor;
        imageComponent.DOKill();
        imageComponent.DOFade(targetAlpha, duration).SetEase(Ease.OutExpo);
    }
}

