﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISectionElementView2D {

    void ChangeColorAlpha(float startAlpha, float targetAlpha, float duration);
	
}
