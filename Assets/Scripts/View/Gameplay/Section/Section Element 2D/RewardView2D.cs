﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RewardView2D : MonoBehaviour, ISectionElementView2D, IRewardView {

	public GameObject[] orbits = new GameObject[3];
    Image[] imageArray;	
	Image imageComponent;

    void Awake() {
        imageArray = new Image[orbits.Length + 1];
		imageArray = GetComponentsInChildren<Image>();
    }

	public void ShowOrbits(int multiplier) {
		switch (multiplier) {
		case 1:
			orbits[0].SetActive(true);
			orbits[1].SetActive(false);
			orbits[2].SetActive(false);
			break;
		case 2:
			orbits[0].SetActive(true);
			orbits[1].SetActive(true);
			orbits[2].SetActive(false);
			break;
		case 3:
			orbits[0].SetActive(true);
			orbits[1].SetActive(true);
			orbits[2].SetActive(true);
			break;
		default:
			orbits[0].SetActive(false);
			orbits[1].SetActive(false);
			orbits[2].SetActive(false);
			break;
		}
	}

    public void ChangeColorAlpha(float startAlpha, float targetAlpha, float duration) {
		Color tempColor = imageArray[0].color;
		tempColor.a = startAlpha;
        for (int i = 0; i < imageArray.Length; i++) {
            imageArray[i].color = tempColor;
			imageArray[i].DOKill();
			imageArray[i].DOFade(targetAlpha, duration).SetEase(Ease.OutExpo);
        }
    }
}
