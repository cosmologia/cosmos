﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SectionView: MonoBehaviour {

    public Transform[] sectionElementTransformArray;
    protected Transform[,] sectionElementPlaceArray;
    public Transform[] obstaclesArray;
    public Transform reward;
    protected List<GameObject> currentSectionElementList = new List<GameObject>();
	
    void Awake() {
        ConvertTo2DArray();
    }

    protected void ConvertTo2DArray()  {
        sectionElementPlaceArray = new Transform[3, 3];
        int tempIterator = 0;
        for (int i = 0; i < sectionElementPlaceArray.GetLength(0) ; i++){
            for (int j = 0; j < sectionElementPlaceArray.GetLength(1) ; j++) {
                sectionElementPlaceArray[i, j] = sectionElementTransformArray[tempIterator];
                tempIterator++;
            }
        }
    }
	
    protected void LocateSectionElements(Section section) { 
        int obstaclesArrayIteratorTemp = 0;
        currentSectionElementList = new List<GameObject>();
        for (int i = 0; i < section.elementArray.GetLength(0) ; i++) { 
            for (int j = 0; j < section.elementArray.GetLength(1) ; j++) {
                if (section.elementArray[i, j] != null) { 
                    if (section.elementArray[i, j].tag == EnumerationHelper.SectionElementTag.Common) {
                        obstaclesArray[obstaclesArrayIteratorTemp].SetParent(sectionElementPlaceArray[i, j]);
                        obstaclesArray[obstaclesArrayIteratorTemp].gameObject.SetActive(true);
                        currentSectionElementList.Add(obstaclesArray[obstaclesArrayIteratorTemp].gameObject);
                        obstaclesArrayIteratorTemp++;
                    } else if (section.elementArray[i, j].tag == EnumerationHelper.SectionElementTag.Reward) {
                        reward.GetComponent<IRewardView>().ShowOrbits(((Reward) section.elementArray[i, j]).multiplier);
                        reward.SetParent(sectionElementPlaceArray[i, j]);
                        reward.gameObject.SetActive(true);
                        currentSectionElementList.Add(reward.gameObject);
                    }
                }
            }
		}
    }

    abstract public void ShowSectionElements(Section section);

    abstract public void HideSectionElements();
}
