﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShipRotation : MonoBehaviour {

	public Transform pivot;
	public ShipSelector shipSelector;
	public Text shipNames;
	public CanvasGroup canvasGroup;
	public Image inputBlockerOverlayImage;
	public MainUI mainUI;

	void Start () {
		AudioView.audioView.PlayMusic (0);
	}

	public void Turn (string _name, Vector3 target) {
		//float t = 0;
		shipNames.DOKill ();
		shipNames.DOText (_name, 1f, true, ScrambleMode.All).SetEase (Ease.InCirc);
		pivot.DOKill ();
		pivot.DORotate (target, 1f)
			.SetEase (Ease.OutCirc)
			.OnComplete (() => {
				inputBlockerOverlayImage.raycastTarget = false;
			});
	}

	public void StartGame () {
		AudioView.audioView.PlaySFX (5);
		string sceneName = (GameManager.firstTimePlaying) ? "Instructions" : "Loadingscreen";
		if (GameManager.firstTimePlaying) GameManager.firstTimePlaying = false;

		mainUI.DoTransitionAnimation(1f, 2f, false);
		DOVirtual.DelayedCall(2f, () => {
				SceneManager.LoadScene (sceneName);
		});
	}
}