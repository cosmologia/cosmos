﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Share : MonoBehaviour {

	// Your app’s unique identifier.
	string AppID = "365391287223417";

	// The link attached to this post.
	public string FacebookLink = "https://www.facebook.com/Cosmos-the-videogame-104388503609668/";

	void ShareScoreOnFB () {
		Application.OpenURL ("https://www.facebook.com/dialog/feed?" + "app_id=" + AppID + "&link=" +
			FacebookLink + "&redirect_uri=https://facebook.com/");
	}

	string ReplaceSpace (string val) {
		return val.Replace (" ", "%20");
	}

	string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	public string TwitterTextToDisplay = "My new score in C.O.S.M.O.S. is: ";

	public void shareScoreOnTwitter () {
		Application.OpenURL (TWITTER_ADDRESS + "?text=" + UnityWebRequest.EscapeURL (TwitterTextToDisplay) +
			Player.player.currentScore.ToString () + "&amp;lang=en");
	}
}