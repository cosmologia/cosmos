﻿using DG.Tweening;
using UnityEngine.UI;

public static class Utils {
    static public void SetText (this Text textComponent, string text, float duration, Ease ease) {
        textComponent.DOKill ();
        textComponent.DOText (text, duration, false, ScrambleMode.All).SetEase (ease).SetUpdate (true);
    }
}