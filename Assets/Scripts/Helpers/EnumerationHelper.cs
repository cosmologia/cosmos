﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnumerationHelper {

    public enum SectionPlace { 
    	TopLeft, Top, TopRight,
    	CenterLeft, Center, CenterRight,
    	BottomLeft, Bottom, BottomRight 
    };

    public enum SectionElementTag{
        Common, Reward, Empty
    };

    public enum SpaceshipName{
        Skylark, Nsonar, Moontaker
    };
    
    public enum SpaceshipSpeedCommands{
		Accelerate, Deaccelerate 
    };
}
